#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils
import re

lines = utils.read_batches('4.1')


def val_or_none(term, parts):
    for p in parts:
        if term in p:
            v = p.split(':', 1)[1]
            return v
    else:
        return None


def validate(value, regex):
    if value:
        pattern = re.compile(regex)
        if pattern.match(value):
            return value
        else:
            return None
    else:
        return None


def parse_line(l):
    parts = l.split(' ')
    byr = validate(val_or_none('byr', parts), '^19[2-9]\d|200[0-2]$')
    iyr = validate(val_or_none('iyr', parts), '^201\d|2020$')
    eyr = validate(val_or_none('eyr', parts), '^202\d|2030$')
    hgt = validate(val_or_none('hgt', parts), '^1[5-8]\dcm|19[0-3]cm|59in|6\din|7[0-6]in$')
    hcl = validate(val_or_none('hcl', parts), '^#[0-9a-f]{6}$')
    ecl = validate(val_or_none('ecl', parts), '^amb|blu|brn|gry|grn|hzl|oth$')
    pid = validate(val_or_none('pid', parts), '^\d{9}$')
    cid = validate(val_or_none('cid', parts), '.*')
    return Passport(byr, iyr, eyr, hgt, hcl, ecl, pid, cid)


class Passport:
    def __init__(self, byr, iyr, eyr, hgt, hcl, ecl, pid, cid):
        self.byr = byr
        self.iyr = iyr
        self.eyr = eyr
        self.hgt = hgt
        self.hcl = hcl
        self.ecl = ecl
        self.pid = pid
        self.cid = cid
        self.missing_values = []
        self.valid = False
        self.is_valid_part_1()

    def is_valid_part_1(self):
        missing = []
        for k, v in self.__dict__.items():
            if len(k) == 3:
                if v is None:
                    missing.append(k)
        self.missing_values = missing
        if missing:
            if len(missing) == 1 and 'cid' in missing:
                self.valid = True
            else:
                self.valid = False
        else:
            self.valid = True


passports = [parse_line(l) for l in lines]
print(len([p for p in passports if p.valid]))

# for p in passports:
#     p.is_valid_part_1()
#     print(f'Passport valid: {p.valid} - Missing values: {p.missing_values}')
