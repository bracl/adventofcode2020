#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy

import utils


class Rule:
    def __init__(self, name, ll, lu, ul, uu):
        self.n = name
        self.ll = ll
        self.lu = lu
        self.ul = ul
        self.uu = uu
        self.pp = []
        self.position = None

    def vn(self):
        return [x for x in range(self.ll, self.lu + 1)] + [x for x in range(self.ul, self.uu + 1)]


rules = utils.read_input('16.1')
my_ticket = utils.read_input('16.2')[0].split(',')
nearby_tickets = utils.read_input('16.3')

valid_numbers = []
rools = []
for r in rules:
    n, c = r.split(':', 1)
    u, l = c.split('or')
    ul, uu = [int(x) for x in u.split('-')]
    ll, lu = [int(x) for x in l.split('-')]
    rools.append(Rule(n, ll, lu, ul, uu))
    for i in range(ul, uu + 1):
        valid_numbers.append(i)
    for j in range(ll, lu + 1):
        valid_numbers.append(j)
valid_numbers = set(valid_numbers)

invalid_numbers = []
valid_tickets = []
for nt in nearby_tickets:
    vals = [int(x) for x in nt.split(',')]
    v = True
    for v in vals:
        if v not in valid_numbers:
            v = False
            break
    if v:
        valid_tickets.append(nt)

print(sum(invalid_numbers))
print(valid_tickets)
vt = [[int(x) for x in t.split(',')] for t in valid_tickets]

fields = list(map(list, zip(*vt)))
print(fields)


def check_field_on_rule(ticket_values, rule):
    vn = set(rule.vn())
    tv = set(ticket_values)
    return vn.issuperset(tv)


def find_positions(fields, rules, taken):
    for r in rules:
        r.pp = []
    for i, f in enumerate(fields):
        if i not in taken:
            for r in rules:
                if check_field_on_rule(f, r):
                    r.pp.append(i)
    return rules


still_nones = True
to_work_out = deepcopy(rools)
position_found = []
taken = []
while still_nones:
    remaining_rules = [r for r in rools if r not in position_found]
    print([r.n for r in remaining_rules])
    if remaining_rules:
        print(f'already Taken {taken}')
        to_work_out = find_positions(fields, remaining_rules, taken)
        print('\n'.join([','.join([str(i) for i in set(r.pp)]) for r in to_work_out]))
        print(len(to_work_out))
        br = 0
        for k, r in enumerate(to_work_out):
            if len(set(r.pp)) == 1:
                pos = r.pp[0]
                r.position = pos
                taken.append(pos)
                position_found.append(r)
        print(taken)

    else:
        still_nones = False

mul = 1
for rp in rools:
    if rp.n.startswith('departure'):
        mul *= int(my_ticket[rp.position])

print(mul)
