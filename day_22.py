#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cProfile
import pstats
from copy import deepcopy

import utils

profile = cProfile.Profile()

lines = utils.read_input('22.1')


def score_cards(cards):
    cards.reverse()
    s = 0
    for i, v in enumerate(cards):
        # print(f'{i + 1} * {v}')
        s += ((i + 1) * v)
    return s


class Game:
    def __init__(self, player1, player2, game, parent_game=0, mini=False):
        self.one = player1
        self.two = player2
        self.states = set()
        self.mini = mini
        self.turn = 0
        self.game = game
        self.parent_game = parent_game
        self.games_spawned = 0
        # print(f'=== Game {self.game} ===\n')

    def check(self):
        if self.one:
            p = 'One'
            s = score_cards(self.one)
        else:
            p = 'Two'
            s = score_cards(self.two)
        # if not self.mini:
        # print(f'Game Over - Player {p} is the winner !!')
        # print(s)
        return p, s

    def check_states(self):
        current_state = f'1-{"".join([str(x) for x in self.one])}:2-{"".join([str(x) for x in self.two])}'
        # current_state = score_cards(self.one) + 1000 * score_cards(self.two)
        if current_state in self.states:
            return False
        else:
            self.states.add(current_state)
            return True

    def play1(self):
        while self.one and self.two:
            self.draw1()
        winner, score = self.check()
        print(winner, score)
        return winner, score

    def play2(self):
        state_seen = False
        while self.one and self.two and not state_seen:
            self.turn += 1
            # print(f'-- Round {self.turn} (Game {self.game}) --')
            state_seen = self.draw2()
        if state_seen:
            winner, score = 'One', 0
        else:
            winner, score = self.check()
        if not self.mini:
            print(winner, score)
        # if self.mini:
        # print(f'...anyway, back to Game {self.parent_game}')
        return winner, score, self.games_spawned

    def draw1(self):
        o = self.one.pop(0)
        t = self.two.pop(0)
        if o > t:
            self.one.append(o)
            self.one.append(t)
        else:
            self.two.append(t)
            self.two.append(o)

    def draw2(self):
        # print(f'Player 1\'s deck: {", ".join([str(x) for x in self.one])}')
        # print(f'Player 2\'s deck: {", ".join([str(x) for x in self.two])}')
        g2g = self.check_states()
        if g2g:
            o = self.one.pop(0)
            t = self.two.pop(0)
            # print(f'Player 1 plays: {o}')
            # print(f'Player 2 plays: {t}')
            if len(self.one) >= o and len(self.two) >= t:
                # print('Playing a sub-game to determine the winner...\n')
                p1d = deepcopy(self.one[:o])
                p2d = deepcopy(self.two[:t])
                assert len(p1d) == o
                assert len(p2d) == t
                self.games_spawned += 1
                mini = Game(p1d, p2d, game=self.game + self.games_spawned, mini=True, parent_game=self.game)
                winner, s, gs = mini.play2()
                self.games_spawned += gs
                if winner in 'One':
                    # print(f'Player 1 wins round {self.turn} of game {self.game}!\n')
                    self.one.append(o)
                    self.one.append(t)
                else:
                    # print(f'Player 2 wins round {self.turn} of game {self.game}!\n')
                    self.two.append(t)
                    self.two.append(o)

            else:
                if o > t:
                    # print(f'Player 1 wins round {self.turn} of game {self.game}!\n')
                    self.one.append(o)
                    self.one.append(t)
                else:
                    # print(f'Player 2 wins round {self.turn} of game {self.game}!\n')
                    self.two.append(t)
                    self.two.append(o)
            return False
        else:
            # o = self.one.pop(0)
            # t = self.two.pop(0)
            # self.one.append(o)
            # self.one.append(t)
            # print('State seem before ending the game with Winner 1')
            return True


p1 = []
p2 = []
switched = False
for l in lines:
    if 'Player' in l:
        pass
    elif l in '':
        switched = True
    elif not switched:
        p1.append(int(l))
    else:
        p2.append(int(l))

print(p1)
print(p2)

# p1 = [9, 2, 6, 3, 1]
# p2 = [5, 8, 4, 7, 10]

# p1 = [43, 19]
# p2 = [2, 29, 14]

g = Game(p1, p2, game=1)

profile.runcall(g.play2)
ps = pstats.Stats(profile)
ps.print_stats()
