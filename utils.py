#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import islice


def read_input(problem, file_extension='.txt'):
    filepath = f'C:\\Users\\Bradley.King\\PycharmProjects\\aoc2020\\aoc_input\\{problem}{file_extension}'
    with open(filepath, 'r')as f:
        return [x.strip() for x in f.readlines()]


def read_ints(problem, file_extension='.txt', sort=False):
    if sort:
        return sorted([int(x) for x in read_input(problem, file_extension)])
    else:
        return [int(x) for x in read_input(problem, file_extension)]


def read_batches(problem, file_extension='.txt'):
    filepath = f'C:\\Users\\Bradley.King\\PycharmProjects\\aoc2020\\aoc_input\\{problem}{file_extension}'
    batches = []
    empty_lines = 0
    with open(filepath, 'r')as f:
        b = ''
        for l in f.readlines():
            if l == '\n':
                empty_lines += 1
                batches.append(b)
                b = ''
            else:
                b += l.replace('\n', ' ')
        batches.append(b)
    # print(len(batches), empty_lines)
    return batches


def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result
