#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('14.1')
# lines = [
#     'mask = 000000000000000000000000000000X1001X',
#     'mem[42] = 100',
#     'mask = 00000000000000000000000000000000X0XX',
#     'mem[26] = 1',
# ]


def apply_mask(mask, value):
    value_as_bits = bin(value).replace('0b', '').zfill(36)
    masked = list(value_as_bits)
    for i, b in enumerate(mask):
        if b not in 'X':
            masked[i] = b
    masked = ''.join(masked)
    decimal = int(masked, 2)
    return decimal, value_as_bits, masked


def apply_mask_part_2(mask, value):
    value_as_bits = bin(value).replace('0b', '').zfill(36)
    # print(f'incoming value {value} -> address {value_as_bits}')
    masked = list(value_as_bits)
    for i, b in enumerate(mask):
        if b in 'X1':
            masked[i] = b
    masked = ''.join(masked)
    print(f'became {masked}')
    addresses = expand(masked)
    return addresses


def expand(mask):
    adds = []

    def combination(m, acc):
        if 'X' not in m:
            return [m]
        elif m in 'X':
            return ['0', '1']
        else:
            head, tail = m.split('X', 1)
            # print(f'splitting {m} to {head} and {tail}')
            o = f'1{tail}'
            z = f'0{tail}'
            oc = combination(o, acc)
            oz = combination(z, acc)
            one = [head + x for x in oc]
            zero = [head + x for x in oz]
            return one + zero

    adds = combination(mask, adds)
    assert len(adds) == pow(2, mask.count('X'))
    return adds


class Memory:
    def __init__(self, lines):
        self.memory = {}
        self.instructions = lines
        self.mask = None

    def update_mask(self, ins):
        self.mask = ins
        print(f'The new mask is {self.mask}')

    def write_to_memory_part1(self, address, value):
        decimal, bbm, bam = apply_mask(self.mask, value)
        res = {
            'vam': decimal,
            'bbm': bbm,
            'vbm': value,
            'bam': bam
        }
        self.memory[address] = res

    def write_to_memory(self, address, value):
        print(f'Writing mem[{address}] = {value}')
        addresses = apply_mask_part_2(self.mask, int(address))
        for add in addresses:
            print(f'Writing value {value} to address {add} (decimal {int(add, 2)})')
            self.memory[add] = value

    def process_instructions(self):
        for l in self.instructions:
            if l.startswith('mask'):
                self.update_mask(l.replace('mask = ', ''))
            elif l.startswith('mem'):
                r = l.replace('mem[', '').replace(']', '')
                nums = [int(s) for s in r.split() if s.isdigit()]
                self.write_to_memory(nums[0], nums[1])

    def sum_memory(self):
        M = 0
        for k, v in self.memory.items():
            M += int(v)
        print(M)


mem = Memory(lines)

mem.process_instructions()
mem.sum_memory()

# expand('010X11X1000101X1XX001110101X101X0111')
# print(len('11011111000101111100111010111011'))
