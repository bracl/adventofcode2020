#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_batches('6.1')

count = 0
for g in lines:
    individuals = [set(x) for x in g.split(' ') if x]
    all = set.intersection(*map(set, individuals))
    count += len(all)

print(count)
