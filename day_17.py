#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('17.1')
# lines = ['.#.',
#          '..#',
#          '###']

active = []


def count_active_neighbours(coord, all_actives):
    a = []
    for w in range(coord[3] - 1, coord[3] + 2):
        for x in range(coord[0] - 1, coord[0] + 2):
            for y in range(coord[1] - 1, coord[1] + 2):
                for z in range(coord[2] - 1, coord[2] + 2):
                    # print(x, y, z)
                    if (x, y, z, w) in all_actives:
                        if (x, y, z, w) != (coord[0], coord[1], coord[2], coord[3]):
                            a.append((x, y, z, w))
    return len(a)


grid_size = len(lines[0])
for x in range(grid_size):
    for y in range(grid_size):
        if lines[y][x] == '#':
            active.append((x, y, 0, 0))

print(len(active))

# for a in active:
#     c = count_active_neighbours(a, all_actives=active)
#     print(f'coord {a} has {c} active neighbours')

x = len(lines[0])
y = len(lines)
z = 1
w = 1
for cycle in range(6):
    print(f'This is the start of cycle {cycle + 1} !!')
    additions = []
    subtractions = []
    for dub in range(-w, w + 1):
        for zed in range(-z, z + 1):
            for why in range(-z, y + 1):
                for ex in range(-z, x + 1):
                    coord = (ex, why, zed, dub)
                    an = count_active_neighbours(coord, active)
                    if coord in active:
                        if an in [2, 3]:
                            # additions.append(coord)
                            pass
                        else:
                            subtractions.append(coord)
                    else:
                        if an in [3]:
                            additions.append(coord)
    cross_over = [i for i in additions if i in subtractions]
    assert len(cross_over) == 0
    for add in additions:
        active.append(add)
    active = [c for c in active if c not in subtractions]
    # print(len(active))
    # for zed in range(-z, z + 1):
    #     for c in active:
    #         if c[2] == zed:
    #             print(c)
    x += 1
    y += 1
    z += 1
    w += 1

print(active)
print(len(active))
