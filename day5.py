#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('5.1')



def parse_transformed_binary(str, zero, one):
    str = str.replace(zero, '0').replace(one, '1')
    col = int(f'0b{str}', base=2)
    return col


def parse_col(str):
    return parse_transformed_binary(str, zero='L', one='R')


def parse_row(str):
    return parse_transformed_binary(str, zero='F', one='B')


def seat_id(str):
    row = str[:7]
    col = str[-3:]
    id = parse_row(row) * 8 + parse_col(col)
    return id


ids = [seat_id(x) for x in lines]

for x in utils.window(sorted(ids)):
    assert x[0] < x[1]
    if x[1] - x[0] != 1:
        print(x[1] - 1)
