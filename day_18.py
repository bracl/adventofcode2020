#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('18.1')

ans = 0


def compute_two_digits(exp):
    if '*' in exp:
        l, r = exp.split('*')
        return int(l) * int(r)
    else:
        l, r = exp.split('+')
        return int(l) + int(r)


def addition_before_multiplication(e):
    if '+' not in e:
        return left_to_right(e)
    else:
        start, rest = e.split('+', 1)
        if ' ' in start.strip():
            start, l = start.strip().rsplit(' ', 1)
        else:
            l = start.strip()
            start = ''
        if ' ' in rest.strip():
            r, rest = rest.strip().split(' ', 1)
        else:
            r = rest.strip()
            rest = ''
        exp = ' '.join(f'{start} {int(l) + int(r)} {rest}'.split())
        return addition_before_multiplication(exp)


def left_to_right(e):
    tmp = e.split(' ', 3)
    if len(tmp) == 1:
        return int(tmp[0])
    next_two_digits = ' '.join(tmp[:3])
    rest = tmp[-1]
    one_next_digit = compute_two_digits(next_two_digits)
    expression = ' '.join(f'{one_next_digit} {rest}'.split())
    if expression.count('+') + expression.count('*') == 0:
        return one_next_digit
    return left_to_right(expression)


def remove_brackets(e):
    assert e.count('(') == e.count(')')
    if e.count('(') == 0:
        return e
    else:
        last_right = e.rfind('(')
        from_last_right = e[last_right + 1:]
        brack, rest = from_last_right.split(')', 1)
        simped = addition_before_multiplication(brack.strip())
        exp = ' '.join(f'{e[:last_right]} {simped} {rest}'.split())
        return remove_brackets(exp)


def solve(expression):
    brackless = ' '.join(remove_brackets(expression).split())
    answer = addition_before_multiplication(brackless)
    return answer


for e in lines:
    v = solve(e)
    ans += v

print(ans)
#
# a = solve('5 + (8 * 3 + 9 + 3 * 4 * 3)')
# print(a)
