#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy
from math import sqrt

import numpy

import utils


class Tile:
    def __init__(self, t, dimx, dimy):
        self.id = int(t.split(':', 1)[0].replace('Tile ', ''))
        self.image = numpy.reshape(numpy.array(list(t.split(':', 1)[1].replace(' ', ''))), (dimx, dimy))

    def rotate(self, degrees):
        rotations = degrees / 90
        tmp = numpy.rot90(self.image, k=rotations, axes=(1, 0))
        self.image = tmp

    def flip(self, h=None, v=None):
        if h:
            self.image = numpy.fliplr(self.image)
        if v:
            self.image = numpy.flipud(self.image)

    def top(self):
        return self.image[0, :]

    def bottom(self):
        return self.image[-1, :]

    def right(self):
        return self.image[:, -1]

    def left(self):
        return self.image[:, 0]

    def image_line(self, line):
        l = self.image[line + 1][1:][:-1]
        s = ''.join(l)
        return s

    def internal_image(self):
        vertical = self.image[:-1][1:]
        horizontal = [''.join(x[:-1][1:]) for x in vertical]
        return horizontal


def remove_tile(tl, ids):
    if isinstance(ids, list):
        return [t for t in tl if t.id not in ids]
    else:
        return [t for t in tl if t.id not in [ids]]


def find_right(tile, remaining_tiles):
    right = tile.right()
    for t in remaining_tiles:
        for r in [0, 90, 180, 270]:
            t.rotate(r)
            for f in [(0, 0), (1, 0), (0, 1), (1, 1)]:
                t.flip(f[0], f[1])
                if ''.join(t.left()) == ''.join(right):
                    return t
    return None


def find_below(tile, remaining_tiles):
    bottom = tile.bottom()
    for t in remaining_tiles:
        for r in [0, 90, 180, 270]:
            t.rotate(r)
            for f in [(0, 0), (1, 0), (0, 1), (1, 1)]:
                t.flip(f[0], f[1])
                if ''.join(t.top()) == ''.join(bottom):
                    return t
    return None


def find_below_right(above, left, remaining_tiles):
    bottom = above.bottom()
    right = left.right()
    for t in remaining_tiles:
        for r in [0, 90, 180, 270]:
            t.rotate(r)
            for f in [(0, 0), (1, 0), (0, 1), (1, 1)]:
                t.flip(f[0], f[1])
                if ''.join(t.top()) == ''.join(bottom) and ''.join(t.left()) == ''.join(right):
                    return t
    return None


def fill_position(image, x, y, remaining_tiles):
    occupied_neighbours = []
    for ii in [(-1, 0), (0, -1), (0, 1), (1, 0)]:
        try:
            xx = x + ii[0]
            yy = y + ii[1]
            if not image[yy][xx] == 0:
                occupied_neighbours.append((xx, yy))
        except IndexError as ie:
            pass
    if 3 > len(occupied_neighbours) > 1:
        return find_below_right(image[y - 1][x], image[y][x - 1], remaining_tiles)
    elif (x, y - 1) in occupied_neighbours:
        return find_below(image[y - 1][x], remaining_tiles)
    elif (x - 1, y) in occupied_neighbours:
        return find_right(image[y][x - 1], remaining_tiles)


def print_image_ids(image):
    for i in image:
        a = []
        for b in i:
            try:
                a.append(b.id)
            except Exception:
                a.append(0)
        print(a)


def fill_image(tiles, rotations, flips, starting_image):
    for j, t in enumerate(tiles):
        print(f'{str(j * 100 / len(tiles))[:5]}%')
        for r in rotations:
            t.rotate(r)
            for f in flips:
                t.flip(f[0], f[1])
                print(f'Trying top left as Tile {t.id}, rotated {r} degrees, flipped {f}')
                I = deepcopy(starting_image)
                remaining_tiles = deepcopy(tiles)
                success = True
                # print(f'number placed {number_placed} - therefore, starting at {xx, yy}')
                I[0][0] = t
                remaining_tiles = remove_tile(remaining_tiles, t.id)
                for i in range(len(tiles)):
                    if success:
                        x = i // image_dimensions
                        y = i % image_dimensions
                        if x == 0 and y == 0:
                            pass
                        else:
                            matching_tile = fill_position(I, x, y, remaining_tiles)
                            if matching_tile:
                                I[y][x] = matching_tile
                                remaining_tiles = remove_tile(remaining_tiles, matching_tile.id)
                            else:
                                success = False
                if success:
                    return I
    return None


tiles = {}
for t in utils.read_batches('20.1'):
    ti = Tile(t, 10, 10)
    tiles[ti.id] = ti

# random.shuffle(tiles)
how_many_tiles = len(tiles)
image_dimensions = int(sqrt(how_many_tiles))
print(f'Creating an image of size {image_dimensions} x {image_dimensions}')

rs = [0, 90, 180, 270]
fs = [(0, 0), (1, 0), (0, 1), (1, 1)]
si = [[0 for col in range(image_dimensions)] for row in range(image_dimensions)]


def part1():
    tiles = [Tile(t, 10, 10) for t in utils.read_batches('20.1')]
    image = fill_image(tiles, rs, fs, si)
    if image:
        for l in image:
            print([t.id for t in l])
        print(image[0][0].id * image[-1][-1].id * image[0][-1].id * image[-1][0].id)
    return image

#
# part_1_image = [[1831, 3607, 2677, 3449, 1451, 1013, 2953, 2999, 2731, 3583, 3529, 1699],
#                 [1423, 3671, 3461, 2971, 3023, 1249, 2687, 3539, 3313, 2593, 3329, 2251],
#                 [1069, 2179, 2903, 2621, 2549, 2083, 3209, 1697, 1657, 2963, 1499, 1367],
#                 [1877, 1973, 3001, 3673, 2897, 3221, 2081, 1693, 2927, 2437, 2939, 3767],
#                 [1871, 3467, 2887, 1433, 3343, 3697, 3413, 1511, 2129, 1951, 2339, 2693],
#                 [3989, 2423, 3931, 1453, 2917, 1949, 3331, 3433, 3911, 1307, 2267, 3929],
#                 [3851, 2053, 3217, 3797, 2089, 3259, 1759, 2341, 3011, 1153, 1123, 2441],
#                 [2087, 3517, 2297, 1087, 2221, 1481, 1459, 1471, 2237, 2281, 2357, 2017],
#                 [1747, 2069, 3041, 3769, 3643, 2459, 2467, 1103, 1279, 2203, 1847, 2011],
#                 [2663, 1171, 1303, 3631, 2311, 3079, 3533, 3919, 1151, 2389, 1789, 1667],
#                 [2633, 1597, 1487, 3547, 3943, 3581, 1039, 1907, 1109, 2671, 3557, 1559],
#                 [2789, 1549, 2381, 3617, 2729, 2647, 2797, 2417, 2029, 2767, 3391, 2309]]


part_1_image = part1()

image_lines = {}


for i in range(12):
    for j in range(12):
        # id = part_1_image[i][j]
        # tile = tiles[id]
        tile = part_1_image[i][j]
        ii = tile.internal_image()
        for k in range(8):
            li = i*8 + k
            if li in list(image_lines):
                image_lines[li] += ii[k]
            else:
                image_lines[li] = ii[k]

c = 0

# for i, ts in enumerate(part_1_image):
#     for j, t in enumerate(ts):
#         tile = tiles[t]
#         for l in range(8):
#             ord = (8 * i + l)
#             if ord in list(image_lines):
#                 image_lines[ord] += tile.image_line(l)
#             else:
#                 image_lines[ord] = tile.image_line(l)

arr = ['Tile 1:']
for k, v in image_lines.items():
    assert len(v) == 12 * 8
    arr.append(v)

# for x in ['...####.#....##...#.#..###.....#.#.#......#.#...#.###...#.##.O#....#....##..#.##.....##..#......'.replace('O', '#'),
#           '.#.....#.#..#..##.......#.#.....#...#....#.O.##.OO#.#.OO.##.OOO####.......#.......#....#.#......'.replace('O', '#'),
#           '....#.#.##.#............#...#...#.#.#......#O.#O#.O##O..O.#O##.########.#.......#......#.#..#...'.replace('O', '#')]:
#     arr.append(x)

# arr = [
#     'Tile 1:',
#     '.#.#..#.##...#.##..#####',
#     '###....#.#....#..#......',
#     '##.##.###.#.#..######...',
#     '###.#####...#.#####.#..#',
#     '##.#....#.##.####...#.##',
#     '...########.#....#####.#',
#     '....#..#...##..#.#.###..',
#     '.####...#..#.....#......',
#     '#..#.##..#..###.#.##....',
#     '#.####..#.####.#.#.###..',
#     '###.#.#...#.######.#..##',
#     '#.####....##..########.#',
#     '##..##.#...#...#.#.#.#..',
#     '...#..#..#.#.##..###.###',
#     '.#.#....#.##.#...###.##.',
#     '###.#...#..#.##.######..',
#     '.#.#.###.##.##.#..#.##..',
#     '.####.###.#...###.#..#.#',
#     '..#.#..#..#.#.#.####.###',
#     '#..####...#.#.#.###.###.',
#     '#####..#####...###....##',
#     '#.##..#..#...#..####...#',
#     '.#.###..##..##..####.##.',
#     '...###...##...#...#..###'
# ]

q = ['...#.#.#.#..#.#..###.....#...#.#..#..#.....#.##...#.####.#......#....##...###....#.....#....#...',
     '...####.#....##...#.#..###.....#.#.#......#.#...#.###...#.##.O#....#....##..#.##.....##..#......'.replace('O', '#'),
     '.#.....#.#..#..##.......#.#.....#...#....#.O.##.OO#.#.OO.##.OOO####.......#.......#....#.#......'.replace('O', '#'),
     '....#.#.##.#............#...#...#.#.#......#O.#O#.O##O..O.#O##.########.#.......#......#.#..#...'.replace('O', '#'),
     '.#...#.###..##..#.#.###.#####......#..#.##.#...#.####.###.##..#...##.#######.#.#..##.....#.....#']

print(''.join(q))

image = Tile(' '.join(arr), len(arr[-1]), 96)
image.flip(1, 0)
image.rotate(270)

image.internal_image()

for l in image.image:
    print(''.join(l))

import re


def seamonster(wrap):
    fw = wrap - 18 - 1
    sw = wrap - 18 - 1
    print(f'Wrapping with {fw}')
    s = '.{18}#.{%s}#.{4}##.{4}##.{4}###.{%s}#..#..#..#..#..#...' % (fw, sw)
    # s = '.{18}O.{%s}O.{4}OO.{4}OO.{4}OOO.{%s}O..O..O..O..O..O...' % (fw, sw)
    return re.compile(s)


reg = seamonster(len(image.image[-1]))
nm = 0
for r in rs:
    image.rotate(r)
    for f in fs:
        image.flip(f[0], f[1])
        as_line = ''.join([''.join(l) for l in image.image])
        monsters = reg.findall(as_line)
        print(len(monsters), as_line)
        if len(monsters) > nm:
            nm = len(monsters)
print(nm)

print(as_line.count('#') - nm * 15)

# print('.{18}#.{5}#.{4}##.{4}##.{4}###.{5}#..#..#..#..#..#...'.replace('#', 'O'))

for i in range(10, 50):
    print(2729 - i*15)