#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('2.1')


# line format "lower-upper character: password"

def is_valid_part1(lower, upper, character, string):
    return lower <= string.count(character) <= upper


def is_valid_part2(pos1, pos2, character, string):
    return (string[pos1-1] == character) != (string[pos2-1] == character)


def parse_password_and_rules(line):
    bounds, character, password = line.split(' ', 3)
    lower, upper = bounds.split('-', 2)
    return int(lower), int(upper), character[:-1], password


valid = []
invalid = []

for l in lines:
    l, u, c, p = parse_password_and_rules(l)
    if is_valid_part2(l, u, c, p):
        valid.append(p)
    else:
        invalid.append(p)

print(len(valid))
