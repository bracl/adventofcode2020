#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from copy import deepcopy

import utils

lines = utils.read_input('8.1')


class Program:
    def __init__(self, lines):
        self.instructions = lines
        self.accumulator = 0
        self.index = 0
        self.previous_indexes = []

    def acc(self, value):
        self.accumulator += int(value)

    def jump(self, value):
        new_index = self.index + int(value)
        self.check_infinite_loop(new_index)
        self.previous_indexes.append(int(self.index))
        self.index = new_index

    def check_infinite_loop(self, proposed_index):
        if proposed_index in self.previous_indexes:
            print('Infinite Loop Detected')
            print(f'Accumulator: {self.accumulator}')
            print(f'Index: {self.index}')
            print(f'self.instructions[self.index]: {self.instructions[self.index]}')
            raise IndexError('Infinite Loop')

    def run_next_instruction(self):
        instruction = self.instructions[self.index]
        if instruction.startswith('acc'):
            self.acc(instruction.split(' ', 1)[1])
            self.jump(1)
        elif instruction.startswith('nop'):
            self.jump(1)
        elif instruction.startswith('jmp'):
            self.jump(instruction.split(' ', 1)[1])
        elif instruction.startswith('brad'):
            print('Program completed !!!')
            print(f'Accumulator: {self.accumulator}')
            sys.exit()


nops = [x for x, y in enumerate(lines) if y.startswith('nop')]
jmps = [x for x, y in enumerate(lines) if y.startswith('jmp')]

# print(nops)
# print(jmps)

for j in jmps:
    lines_copy = deepcopy(lines)
    # lines_copy[n] = lines[n].replace('nop', 'jmp')
    lines_copy[j] = lines[j].replace('jmp', 'nop')
    try:
        p = Program(lines_copy)
        while True:
            p.run_next_instruction()
    except IndexError as ie:
        pass
