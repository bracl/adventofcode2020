#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.close('all')

# path = '''C:\\Users\\Bradley.King\\Downloads\\2020-12-08- CST.csv'''
path = '''C:\\Users\\Bradley.King\\Downloads\\CST-2020-12-18.csv'''
path2 = '''C:\\Users\\Bradley.King\\Downloads\\CST-2021-01-08.csv'''
path3 = '''C:\\Users\\Bradley.King\\Downloads\\CST-2021-01-06.csv'''


if __name__ == '__main__':
    df = pd.read_csv(path)
    print(list(df))
    b = df[::-1]
    df['Download Moving Average'] = df['Download (Rx) bandwidth (bps)'].rolling(window=7).mean()
    df['Upload Moving Average'] = df['Upload (Tx) bandwidth (bps)'].rolling(window=7).mean()
    df['Download Packet Loss'] = df['Rx packet loss (percent)'].rolling(window=7).mean()
    df['Upload Packet Loss'] = df['Tx packet loss (percent)'].rolling(window=7).mean()
    # df.plot(x='Local time', y='Download Moving Average')
    # df.plot(x='Local time', y='Upload Moving Average')
    df.plot(x='Local time', y='Download Packet Loss')
    # df.plot(x='Local time', y='Upload Packet Loss')
    plt.gca().invert_xaxis()
    plt.show()
    #
    # ts = pd.Series(np.random.randn(1000),index=pd.date_range('1/1/2000', periods=1000))
    # ts = ts.cumsum()
    # ts.plot()

