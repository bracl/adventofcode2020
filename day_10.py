#!/usr/bin/env python
# -*- coding: utf-8 -*-

from operator import mul
from collections import Counter
from functools import reduce
from math import factorial

import utils

lines = utils.read_ints('10.1')
# lines = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]


class Chain:
    def __init__(self, adapters, starting_jolt=0, max_joltage_jump=3):
        self.adapters = sorted(adapters)
        self.joltage = starting_jolt
        self.jj = max_joltage_jump
        self.chain = []
        self.joltage_jumps = []

    def find_next_adapter(self):
        potential_adapters = [x for x in self.adapters if self.joltage < x <= self.joltage + self.jj]
        if potential_adapters:
            next_adapter = min(potential_adapters)
            self.joltage_jumps.append(next_adapter - self.joltage)
            self.chain.append(next_adapter)
            self.joltage = next_adapter
            return True
        else:
            if self.joltage >= max(self.adapters):
                print(f'You are using the adapter with the biggest jolt rating - congrats !!')
                print('Adding your devices built-in adapater jump')
                self.joltage_jumps.append(self.jj)
                self.chain.append(self.joltage + self.jj)
                return False
            else:
                print(f'No available adapters found. current joltage {self.joltage} , '
                      f'next adapters {[x for x in self.adapters if x > self.joltage][:5]}')


def ways(total, coins):
    ways = [[Counter()]] + [[] for _ in range(total)]
    for coin in coins:
        for i in range(coin, total + 1):
            ways[i] += [way + Counter({coin: 1}) for way in ways[i - coin]]
    return ways[total]


def npermutations(l):
    num = factorial(len(l))
    mults = Counter(l).values()
    den = reduce(mul, (factorial(v) for v in mults), 1)
    return num / den


def counter_to_list(counted):
    ones = counted[1]
    twos = counted[2]
    threes = counted[3]
    val = [1] * ones + [2] * twos + [3] * threes
    return val


def chain_of_ones(ones):
    possible_perms = []
    number_of_ways = ways(ones, [1, 2, 3])
    for way in number_of_ways:
        perms = npermutations(counter_to_list(way))
        possible_perms.append(perms)
    return int(sum(possible_perms))


c = Chain(lines)
still_adapaters = True

while still_adapaters:
    still_adapaters = c.find_next_adapter()

print(f'Adapater Chain: {c.chain}')
print(f'Joltage Jumps: {c.joltage_jumps}')

print(f'1s {c.joltage_jumps.count(1)} * 3s {c.joltage_jumps.count(3)} = {c.joltage_jumps.count(1) * c.joltage_jumps.count(3)}')

collect = []
one_counter = 0
for x in c.joltage_jumps:
    if x == 3:
        previous_chain = chain_of_ones(one_counter)
        one_counter = 0
        collect.append(previous_chain)
        collect.append(1)
    if x == 1:
        one_counter += 1

print(collect)
print(reduce(mul, collect, 1))


