#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import utils

lines = utils.read_input('7.1')

# lines = ['shiny gold bags contain 2 dark red bags.',
#          'dark red bags contain 2 dark orange bags.',
#          'dark orange bags contain 2 dark yellow bags.',
#          'dark yellow bags contain 2 dark green bags.',
#          'dark green bags contain 2 dark blue bags.',
#          'dark blue bags contain 2 dark violet bags.',
#          'dark violet bags contain no other bags.',
#          ]

# lines = ['shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
#          'faded blue bags contain no other bags.',
#          'dotted black bags contain no other bags.',
#          'vibrant plum bags contain 5 faded blue bags and 6 dotted black bags.',
#          'dark olive bags contain 3 faded blue bags and 4 dotted black bags.', ]

all_bags_child_to_parent = {}
all_bags_parent_to_child = {}
sg = 'shiny gold'


def primary_bag(l):
    return str(re.search(r'^(.*?)bags', l).group(1)).strip()


def inside_bags(l):
    return re.findall(r'(\d.*?)\sbag', l)


def can_contain(colour, rules):
    try:
        return rules[colour]
    except KeyError as ke:
        return []


for l in lines:
    p = primary_bag(l)
    i = inside_bags(l)
    for b in i:
        num, colour = b.split(' ', maxsplit=1)
        if colour in list(all_bags_child_to_parent):
            tmp = all_bags_child_to_parent[colour]
            to_add = {
                'col': p,
                'num': int(num)
            }
            tmp.append(to_add)
            all_bags_child_to_parent[colour] = tmp
        else:
            all_bags_child_to_parent[colour] = [{
                'col': p,
                'num': int(num)
            }]
"""Rules are in the format:

    shiny gold bags contain 2 dark red bags.
    rules['dark red'] = [{'col': 'shiny gold', 'num': 2}]

    child -> parent and how many children"""

for l in lines:
    p = primary_bag(l)
    i = inside_bags(l)
    children = []
    for b in i:
        num, colour = b.split(' ', maxsplit=1)
        children.append({
            'num': int(num),
            'col': colour
        })
    all_bags_parent_to_child[p] = children


def recursive_can_contain(colour, rules):
    """
    This function will find bags which contain the colour provided
    This looks up the rules to find larger and larger bags
    """

    def loop(bag_list, colour):
        parents = can_contain(colour, rules)
        if parents:
            bag_list += parents
            for p in parents:
                bag_list = loop(bag_list, p['col'])
        else:
            return bag_list
        return bag_list

    bag_list = loop([], colour)
    return bag_list


def number_of_children(colour, rules):
    try:
        return sum(x['num'] for x in rules[colour])
    except KeyError as ke:
        return 1


def recursive_contains_how_many(colour, rules):
    """
    This function will find how many bags are inside another bag
    This looks down the rules, into smaller and smaller bags
    """

    # 1 + 1 * 7 + 2 + 2 * 11

    def loop(colour, how_many):
        children = rules[colour]
        if children:
            return how_many + how_many * sum([loop(x['col'], x['num']) for x in children])
        else:
            return how_many + how_many * 0

    return loop(sg, 1)


# bags = recursive_can_contain(sg, all_bags_child_to_parent)
# print(bags)
# print(len(set([x['col'] for x in bags])))
# print(all_bags_parent_to_child)
# print(recursive_contains_how_many(sg, all_bags_parent_to_child))

print(recursive_contains_how_many(sg, all_bags_parent_to_child) - 1)
