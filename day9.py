#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = [int(x) for x in utils.read_input('9.1')]

preamble_length = 25


def slice_preamble(numbers, index, length):
    return numbers[index - length:index]


def rolling_windows(numbers, target):
    for i in range(2, len(numbers)+1):
        windows = utils.window(numbers, i)
        for w in windows:
            if sum(w) == target:
                return w
    else:
        print('Couldnt find a window which matched the target')
        return None


# for x, l in enumerate(lines):
#     if x < preamble_length:
#         pass
#     else:
#         preamble = slice_preamble(lines, x, preamble_length)
#         # print(len(preamble), lines[x], preamble[0], preamble[-1])

window = rolling_windows(lines, 1504371145)
if window:
    print('Found a window')
    print(window)
    print(sorted(window)[0] + sorted(window)[-1])
