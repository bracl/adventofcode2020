#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('3.1')


class Toboggan:
    def __init__(self, terrain):
        self.terrain = terrain
        self.r = 0
        self.c = 0
        self.width = len(self.terrain[0])
        self.height = len(self.terrain)
        self.trees_hit = 0

    def down(self, int=1):
        self.r += int

    def complete(self):
        return self.r >= self.height - 1

    def right(self, int=1):
        next_column = self.c + int
        if next_column >= self.width:
            next_column -= self.width
        self.c = next_column

    def tree(self, r=None, c=None):
        if r is None:
            r = self.r
        if c is None:
            c = self.c
        pos = self.terrain[r][c]
        if '#' in pos:
            self.trees_hit += 1
            return True
        else:
            return False

    def move(self, right, down):
        self.right(right)
        self.down(down)
        self.tree()
        return self.complete()


def slope_run(t, right, down):
    still_going = True
    while still_going:
        finished = t.move(right, down)
        still_going = not finished

    print(f'Trees hit: {t.trees_hit}')
    return t.trees_hit


total_trees = 1
for x, y in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
    t = Toboggan(lines)
    total_trees = total_trees * slope_run(t, x, y)

print(f'multiply total trees: {total_trees}')
