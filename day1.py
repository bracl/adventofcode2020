#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = [int(x) for x in utils.read_input('1.1')]


def two_numbers(number_list, target, print_output=False):
    for i, number in enumerate(number_list[:-1]):
        complementary = target - number
        if complementary in number_list[i + 1:]:
            if print_output:
                print(f'Solution found for two numbers: {number} and {complementary}')
                print(f'{number} * {complementary} = {number * complementary}')
            return number, complementary
    else:
        # print("No solutions exist")
        return None, None


def three_numbers(number_list, target):
    for i, number in enumerate(number_list[:-1]):
        complementary = target - number
        second, third = two_numbers(number_list, complementary)
        if second:
            print(f'Solution Found: {number}, {second} and {third}')
            print(f'{number} * {second} * {third} = {number * second * third}')
            return number, second, third


if __name__ is '__main__':
    three_numbers(lines, 2020)
