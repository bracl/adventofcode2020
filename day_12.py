#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('12.1')
# lines = ['F10', 'N3', 'F7', 'R90', 'F11']

class Ship:
    def __init__(self, instructions):
        self.instructions = instructions
        self.heading = 90
        self.waypoint_east = 10
        self.ship_east = 0
        self.waypoint_north = 1
        self.ship_north = 0

    def parse(self, instruction):
        direction, amount = instruction[0], instruction[1:]
        return direction, int(amount)

    def swivel(self, direction, amount):
        if amount == 180:
            self.waypoint_north *= -1
            self.waypoint_east *= -1
        elif direction+str(amount) in 'L270':
            self.swivel('R', 90)
        elif direction+str(amount) in 'R270':
            self.swivel('L', 90)
        elif direction in 'L':
            tmp = self.waypoint_north
            self.waypoint_north = self.waypoint_east
            self.waypoint_east = -tmp
        elif direction in 'R':
            tmp = self.waypoint_north
            self.waypoint_north = - self.waypoint_east
            self.waypoint_east = tmp
        else:
            print(f'Swivel has been given a direction of {direction} - this should never happen')
            new = 0
        # self.heading = new % 360

    def forward(self, amount):
        self.ship_east += (amount * self.waypoint_east)
        self.ship_north += (amount * self.waypoint_north)
        # if self.heading == 0:
        #     self.ship_north += amount
        # elif self.heading == 90:
        #     self.ship_east += amount
        # elif self.heading == 180:
        #     self.ship_north -= amount
        # elif self.heading == 270:
        #     self.ship_east -= amount

    def direction(self, direction, amount):
        if direction in 'N':
            self.waypoint_north += amount
        if direction in 'E':
            self.waypoint_east += amount
        if direction in 'S':
            self.waypoint_north -= amount
        if direction in 'W':
            self.waypoint_east -= amount

    def manhattan(self):
        return abs(self.ship_east) + abs(self.ship_north)

    def shiver_me_timbers(self):
        for i in self.instructions:
            print(f'ship: N{self.ship_north} E{self.ship_east}  waypoint: N{self.waypoint_north} E{self.waypoint_east}')
            d, a = self.parse(i)
            if d in 'LR':
                self.swivel(d, a)
            elif d in 'NSEW':
                self.direction(d, a)
            elif d in 'F':
                self.forward(a)
        print(self.manhattan())


s = Ship(lines)
s.shiver_me_timbers()
