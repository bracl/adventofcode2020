#!/usr/bin/env python
# -*- coding: utf-8 -*-


from copy import deepcopy

import utils

lines = [list(x) for x in utils.read_input('11.1')]

# lines = [list(x) for x in [
#     'L.LL.LL.LL',
#     'LLLLLLL.LL',
#     'L.L.L..L..',
#     'LLLL.LL.LL',
#     'L.LL.LL.LL',
#     'L.LLLLL.LL',
#     '..L.L.....',
#     'LLLLLLLLLL',
#     'L.LLLLLL.L',
#     'L.LLLLL.LL',
# ]]
# lines = [list(x) for x in [
#     '#.##.##.##',
#     '#######.##',
#     '#.#.#..#..',
#     '####.##.##',
#     '#.##.##.##',
#     '#.#####.##',
#     '..#.#.....',
#     '##########',
#     '#.######.#',
#     '#.#####.##',
# ]]


# lines = [list(x) for x in [
#     '#.L#.L#.L#',
#     '#LLLLLL.LL',
#     'L.L.L..#..',
#     '##L#.#L.L#',
#     'L.L#.#L.L#',
#     '#.L####.LL',
#     '..#.#.....',
#     'LLL###LLL#',
#     '#.LLLLL#.L',
#     '#.L#LL#.L#',
# ]]


class Boat:
    def __init__(self, seats):
        self.seats = seats
        self.height = len(seats)
        self.width = len(seats[0])

    def immediately_occupied(self, row, column, seats):
        count = 0
        # print(row, [y for y in range(row - 1, row + 2) if 0 <= y < self.height], column,
        # [x for x in range(column - 1, column + 2) if 0 <= x < self.width])
        for i in [y for y in range(row - 1, row + 2) if 0 <= y < self.height]:
            for j in [x for x in range(column - 1, column + 2) if 0 <= x < self.width]:
                if i == row and j == column:
                    pass
                elif seats[i][j] in '#':
                    count += 1
        return count

    def diagonal(self, matrix, col, row, up=False, vertical_step=1, right=True, horizontal_step=1, include_start=False):
        row_indexes = []
        col_indexes = []
        while 0 <= row < len(matrix):
            row_indexes.append(row)
            if up:
                row -= vertical_step
            else:
                row += vertical_step
        while 0 <= col < len(matrix[0]):
            col_indexes.append(col)
            if right:
                col += horizontal_step
            else:
                col -= horizontal_step
        if not include_start:
            del row_indexes[0]
            del col_indexes[0]
        indexes = [x for x in zip(row_indexes, col_indexes)]
        diag = []
        for i in indexes:
            diag.append(matrix[i[0]][i[1]])
        return diag

    def visibly_occupied(self, row, column, seats):
        up = [x[column] for i, x in enumerate(seats) if i < row]
        down = [x[column] for i, x in enumerate(seats) if i > row]
        left = seats[row][:column]
        right = seats[row][column + 1:]
        down_right = self.diagonal(seats, column, row, up=False, right=True)
        up_right = self.diagonal(seats, column, row, up=True, right=True)
        down_left = self.diagonal(seats, column, row, up=False, right=False)
        up_left = self.diagonal(seats, column, row, up=True, right=False)

        def a_before_b_in_ll(a, b, ll):
            if ll is None:
                return False
            elif a not in ll:
                return False
            elif a not in ll and b not in ll:
                return False
            elif a in ll and b not in ll:
                return True
            else:
                return ll.index(a) < ll.index(b)

        occupied_seats_visibla = ['up',
                                  'dow',
                                  'lef',
                                  'rig',
                                  'dori',
                                  'upri',
                                  'dole',
                                  'uple']

        occupied_seats_visible = [
            a_before_b_in_ll('#', 'L', up[::-1]),
            a_before_b_in_ll('#', 'L', down),
            a_before_b_in_ll('#', 'L', left[::-1]),
            a_before_b_in_ll('#', 'L', right),
            a_before_b_in_ll('#', 'L', down_right),
            a_before_b_in_ll('#', 'L', up_right),
            a_before_b_in_ll('#', 'L', down_left),
            a_before_b_in_ll('#', 'L', up_left)
        ]
        return sum(occupied_seats_visible)

    def iterate(self):
        changes_made = False
        state = deepcopy(self.seats)
        for i in range(self.height):
            for j in range(self.width):
                seat = self.seats[i][j]
                if seat in '.':
                    pass
                else:
                    # surrounding = self.immediately_occupied(i, j, state)
                    visible = self.visibly_occupied(i, j, state)
                    if seat in 'L' and visible == 0:
                        self.seats[i][j] = '#'
                        changes_made = True
                    if seat in '#' and visible >= 5:
                        self.seats[i][j] = 'L'
                        changes_made = True
        return changes_made

    def occupied_seats(self):
        return sum([x.count('#') for x in self.seats])

    def print(self):
        msg = '\n'.join([''.join(y) for y in self.seats]) + '\n'
        print(msg)


b = Boat(lines)
b.print()
stable = False
counter = 1

while not stable:
    made_changes = b.iterate()
    print(f'Iteration {counter}, occupied seats: {b.occupied_seats()}')
    b.print()
    stable = not made_changes
    counter += 1

print(f'Number of occupied seats: {b.occupied_seats()}')

# a = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
# print(a)
# print(b.diagonal(a, 0, 3, up=True, right=True))
