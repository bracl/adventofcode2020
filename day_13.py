#!/usr/bin/env python
# -*- coding: utf-8 -*-

from operator import itemgetter

import utils

lines = utils.read_input('13.1')

timestamp = int(lines[0])
bus_ids = [int(y) for y in lines[1].split(',') if y != 'x']


def waiting_time(timestamp, bus_ids):
    print(f'Timestamp: {timestamp}')
    for x in bus_ids:
        before = (timestamp // x) * x
        after = before + x
        print(f'bus {x} will arrive at {before} which is {timestamp - before} minutes too early')
        print(f'and then at, {after} which is {after - timestamp} after you arrive')
    wait_times = [(((timestamp // x) + 1) * x) - timestamp for x in bus_ids]
    ordered = sorted(zip(wait_times, bus_ids), key=itemgetter(0))
    return ordered


def how_long_after(bus_ids):
    bus_ids = bus_ids.split(',')
    buses = []
    for b in bus_ids:
        if b not in 'x':
            buses.append((int(b), bus_ids.index(b)))
    return buses


def time_of_arrival_after(t, bus_id):
    return ((((t // bus_id) * bus_id) + bus_id) - t) % bus_id


def check_arrival_times(buses, t):
    arrival_times = [time_of_arrival_after(t, b[0]) for b in buses]
    return arrival_times == [b[1] for b in buses]


# times = waiting_time(timestamp, bus_ids)

# bus_ids_with_xs = '67,x,7,59,61'.split(',')

# bus_ids_with_xs = '67,7,59,61'.split(',')


def bbb(buses, t):
    do_depart = []
    for b in buses:
        if not bus_departs(b[0], t + b[1]):
            return False
        else:
            do_depart.append(b)
    return True


def bus_departs(id, t):
    return t % id == 0


# t = 100000000000000
# while True:
#     if bbb(buses, t):
#         print(t)
#         sys.exit()
#     else:
#         print(f'nah {t}')
#     t += buses[0][0]

# x = 100000000000000
#
# while True:
#     a = check_arrival_times(buses, x)
#     if a:
#         print(x)
#         sys.exit()
#     else:
#         print(f'nah')


from functools import reduce


def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a * b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1


msg = lines[1]
# msg = '17,x,13,19'
# # msg = '7,13,x,x,59,x,31,19'
# msg = '67,x,7,59,61'
# msg = '1789,37,47,1889'

buses = how_long_after(msg)
print(buses)

for b in buses:
    print(f'x = {b[1]} (mod {b[0]})')

n = [x[0] for x in buses]
a = [x[0] - x[1] for x in buses]
t1 = chinese_remainder(n, a)
print(t1)
# t1 = 1068781
for b in buses:
    print(f'id: {b[0]}  offset: {b[1]}    t1%id = {t1 % b[0]}')

print(t1)
print(check_arrival_times(buses, t1))
