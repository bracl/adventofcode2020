#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from math import sqrt

import utils

lines = utils.read_input('24.1')
# lines = [
#     'sesenwnenenewseeswwswswwnenewsewsw',
#     'neeenesenwnwwswnenewnwwsewnenwseswesw',
#     'seswneswswsenwwnwse',
#     'nwnwneseeswswnenewneswwnewseswneseene',
#     'swweswneswnenwsewnwneneseenw',
#     'eesenwseswswnenwswnwnwsewwnwsene',
#     'sewnenenenesenwsewnenwwwse',
#     'wenwwweseeeweswwwnwwe',
#     'wsweesenenewnwwnwsenewsenwwsesesenwne',
#     'neeswseenwwswnwswswnw',
#     'nenwswwsewswnenenewsenwsenwnesesenew',
#     'enewnwewneswsewnwswenweswnenwsenwsw',
#     'sweneswneswneneenwnewenewwneswswnese',
#     'swwesenesewenwneswnwwneseswwne',
#     'enesenwswwswneneswsenwnewswseenwsese',
#     'wnwnesenesenenwwnenwsewesewsesesew',
#     'nenewswnwewswnenesenwnesewesw',
#     'eneswnwswnwsenenwnwnwwseeswneewsenese',
#     'neswnwewnwnwseenwseesewsenwsweewe',
#     'wseweeenwnesenwwwswnew',
# ]

sq3o2 = sqrt(3) / 2


def vvv(pot):
    return round(pot / sq3o2) * sq3o2


T = {}
B = set()


# id_counter = 0
#
# flips_to_black = 0
# flips_to_white = 0
#
#
# def opposite(dir):
#     if dir in 'e':
#         return 'w'
#     elif dir in 'w':
#         return 'e'
#     elif dir in 'ne':
#         return 'sw'
#     elif dir in 'sw':
#         return 'ne'
#     elif dir in 'nw':
#         return 'se'
#     elif dir in 'se':
#         return 'nw'
#     else:
#         return 'Fucked up'
#
#
# def neighbours(dir):
#     if dir in 'e':
#         return ['ne', 'se']
#     elif dir in 'w':
#         return ['nw', 'sw']
#     elif dir in 'ne':
#         return ['e', 'nw']
#     elif dir in 'sw':
#         return ['w', 'se']
#     elif dir in 'nw':
#         return ['ne', 'w']
#     elif dir in 'se':
#         return ['sw', 'e']
#     else:
#         return 'Fucked up'
#
#
# def perform_last_action(tile, B):
#     if tile.coord() in B:
#         B.remove(tile.coord())
#     else:
#         B.add(tile.coord())
#
#
# class Tile(object):
#     def __init__(self, t, x, y, z):
#         self.id = t
#         self.x = x
#         self.y = y
#         self.z = z
#
#     def coord(self):
#         return self.x, self.y, self.z
#
#     def east(self):
#         return self.x + 1, self.y - 1, self.z
#
#     def west(self):
#         return self.x - 1, self.y + 1, self.z
#
#     def northeast(self):
#         return self.x + 1, self.y, self.z - 1
#
#     def southeast(self):
#         return self.x, self.y - 1, self.z + 1
#
#     def northwest(self):
#         return self.x, self.y + 1, self.z - 1
#
#     def southwest(self):
#         return self.x - 1, self.y, self.z + 1
#
#     def switch(self, action):
#         A = {
#             'e': self.east,
#             'w': self.west,
#             'sw': self.southwest,
#             'se': self.southeast,
#             'ne': self.northeast,
#             'nw': self.northwest
#         }
#         return A.get(action)
#
#     def follow_direction(self, direction, B, last_action=False):
#         global id_counter
#         global T
#         move = self.switch(direction)()
#         if T.get(move):
#             tile = T.get(move)
#         else:
#             id_counter += 1
#             tile = Tile(id_counter, move[0], move[1], move[2])
#             T[move] = tile
#         if last_action:
#             perform_last_action(tile, B)
#         return tile
#
#
# center = Tile(id_counter, 0, 0, 0)
# T[center.coord()] = center
#
#
# def walk_path(start, actions):
#     if actions[:2] in ['ne', 'nw', 'se', 'sw']:
#         next_action = actions[:2]
#         actions = actions[2:]
#     else:
#         next_action = actions[0]
#         actions = actions[1:]
#     if actions:
#         tile = start.follow_direction(next_action)
#         # print(f'{next_action} -> {tile.id} : {actions}')
#         return walk_path(tile, actions)
#     else:
#         tile = start.follow_direction(next_action, last_action=True)
#         # print(f'{next_action} -> {tile.id} : {actions}')
#         c = 'white' if tile.coord() in B else 'black'
#         return c
#         # print(f'Finished the walk on tile {tile.id}, colour now {c}')


def not_my_part1(lines):
    B = set()
    for l in lines:
        x, y, z = 0, 0, 0
        y = 0
        while l:
            if l.startswith('e'):
                x += 1
                y -= 1
                l = l[1:]
            elif l.startswith('se'):
                y -= 1
                z += 1
                l = l[2:]
            elif l.startswith('sw'):
                x -= 1
                z += 1
                l = l[2:]
            elif l.startswith('w'):
                x -= 1
                y += 1
                l = l[1:]
            elif l.startswith('nw'):
                z -= 1
                y += 1
                l = l[2:]
            elif l.startswith('ne'):
                x += 1
                z -= 1
                l = l[2:]
            else:
                assert False
        c = (x, y, z)
        if c in B:
            B.remove(c)
        else:
            B.add(c)
    return B


#
# def part1(ll):
#     fw = 0
#     fb = 0
#     for l in ll:
#         c = walk_path(center, l)
#         if c.startswith('w'):
#             fw += 1
#         else:
#             fb += 1
#     bs = len(B)
#     print(f'Number of Black tiles: {bs}')
#     print(f'fw: {fw} fb: {fb}')
#     return bs


# def get_neighbours(read_only, tile):
#     global id_counter
#     neighbours = []
#     nn = 0
#     additions = []
#     for dir in ['e', 'w', 'sw', 'se', 'ne', 'nw']:
#         move = tile.switch(dir)()
#         if read_only.get(move):
#             neighbours.append(read_only.get(move))
#         else:
#             id_counter += 1
#             t = Tile(id_counter, move[0], move[1], move[2])
#             additions.append(t)
#             nn += 1
#     return neighbours, additions, nn


def part2(B):
    for _ in range(100):
        newB = set()
        check = set()
        for t in B:
            check.add(t)
            for (dx, dy, dz) in [(1, -1, 0), (0, -1, 1), (-1, 0, 1), (-1, 1, 0), (0, 1, -1), (1, 0, -1)]:
                check.add((t[0] + dx, t[1] + dy, t[2] + dz))
        for t in check:
            bn = 0
            for (dx, dy, dz) in [(1, -1, 0), (0, -1, 1), (-1, 0, 1), (-1, 1, 0), (0, 1, -1), (1, 0, -1)]:
                if (t[0] + dx, t[1] + dy, t[2] + dz) in B:
                    bn += 1
            if t in B and (bn == 1 or bn == 2):
                newB.add(t)
            if t not in B and bn == 2:
                newB.add(t)
        B = newB
        print(f'{len(B)}')

    return len(B)


# part1(lines)
B = not_my_part1(lines)

# print(len(a))
# print(len(T))

d = time.time()
part2(B)
print(time.time() - d)
