#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy

cups = [int(x) for x in list('476138259')]
ll = len(cups)
m = 0
current_cup = cups[0]


class Node(object):
    def __init__(self, parent, v, prev, next_):
        self.parent = parent
        self.v = v
        self.prev = prev
        self.next = next_

    def insert(self, x):
        node = Node(self.parent, x, self, self.next)
        self.parent.D[x] = node
        self.next = node
        node.next.prev = node
        return node

    def erase(self):
        if self.parent.head == self:
            self.parent.head = self.next
        self.next.prev = self.prev
        self.prev.next = self.next
        del self.parent.D[self.v]


class LinkedList(object):
    def __init__(self):
        self.head = None
        self.D = {}

    def append(self, x):
        if self.head is None:
            node = Node(self, x, None, None)
            node.next = node
            node.prev = node
            self.head = node
            self.D[x] = node
        else:
            node = Node(self, x, self.head.prev, self.head)
            self.head.prev.next = node
            self.head.prev = node
            self.D[x] = node

    def find(self, x):
        return self.D[x]


def pick_up(l, cc):
    i = l.index(cc)
    pu = []
    zero = False
    for _ in range(3):
        ai = (i + 1) % len(l)
        if ai == 0:
            zero = True
        if zero:
            pu.append(l.pop(0))
        else:
            pu.append(l.pop(ai))
    # a = l.pop()
    # b = l.pop((i + 1) % len(l))
    # c = l.pop((i + 1) % len(l))
    return l, pu


def destination(l, cc):
    i = l.index(cc)
    L = deepcopy(l)
    L.sort()
    dest_val = L[L.index(l[i]) - 1]
    dest_index = l.index(dest_val)
    return dest_index, dest_val


def put_down(l, pu, i):
    s = l[:i + 1]
    m = pu
    e = l[i + 1:]
    return s + m + e


def part1(m, cups, current_cup):
    cc = deepcopy(current_cup)
    cs = deepcopy(cups)
    while m < 100:
        cs, pu = pick_up(cs, cc)
        desti, destv = destination(cs, cc)
        cs = put_down(cs, pu, desti)
        cc = cs[(cs.index(cc) + 1) % ll]
        m += 1
    print(cs)
    print(cc)
    return cups, current_cup


part1(m, cups, current_cup)


def part2():
    ll = LinkedList()
    for x in cups:
        ll.append(x)
    next = 10
    while len(ll.D) < int(1e6):
        ll.append(next)
        next += 1
    assert next == int(1e6) + 1
    t = 0
    current = ll.head
    for _ in range(int(10e6)):
        t += 1
        if t % 10000 == 0:
            print(t)
        cn = current.v
        pu = []
        pun = current.next
        for _ in range(3):
            pu.append(pun.v)
            tmp = pun.next
            pun.erase()
            pun = tmp
        dest = cn - 1
        if dest <= 0:
            dest = int(1e6)
        while dest in pu:
            dest -= 1
            if dest <= 0:
                dest = int(1e6)

        dn = ll.find(dest)
        for p in pu:
            dn = dn.insert(p)
        current = ll.find(cn)
        current = current.next

    one = ll.find(1)
    print(one.next.v * one.next.next.v)

part2()