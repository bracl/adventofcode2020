#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Recite:
    def __init__(self, starting):
        self.recent = 0
        self.starting = starting
        self.turn = 1
        self.store = {}

    def start(self):
        for x in self.starting:
            self.update_store(x, self.turn)
            self.turn += 1

    def update_store(self, num, turn):
        if num in self.store:
            diff = self.turn - self.store[num]
            self.store[num] = turn
            self.recent = diff
        else:
            self.store[num] = turn
            self.recent = 0

    def say(self, num):
        self.recent = num
        self.update_store(num, self.turn)

    def take_turn(self):
        self.update_store(self.recent, self.turn)
        self.turn += 1


# r = Recite([6, 4, 12, 1, 20, 0, 16])
r = Recite([0,3,6])

r.start()
print(r.store)
for i in range(2013):
    r.take_turn()

print(r.recent)


class NumbersGame:
    def __init__(self):
        self.cache = dict()
        self.recent = 0
        self.turn = 0

    def play(self, start, stop):
        self.cache = dict()
        self.turn = 1
        for s in start[:-1]:
            self.cache[s] = self.turn
            self.turn += 1
        self.recent = start[-1]
        self.turn += 1
        print(self.cache)
        while self.turn <= stop:
            if self.recent not in self.cache:
                self.cache[self.recent] = self.turn - 1
                self.recent = 0
            else:
                diff = self.turn - 1 - self.cache[self.recent]
                self.cache[self.recent] = self.turn - 1
                self.recent = diff
            self.turn += 1
        return self.recent


def part_1(data):
    ng = NumbersGame()
    return ng.play(data, 2020)


def part_2(data):
    ng = NumbersGame()
    return ng.play(data, 30000000)


data = [6, 4, 12, 1, 20, 0, 16]

test = [0, 3, 6]

# print(part_2(data))
