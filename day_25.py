#!/usr/bin/env python
# -*- coding: utf-8 -*-

lines = [
    8335663,
    8614349
]
loopnumbers = [
    6041182,
    8306868
]


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m


mod = 20201227


def inverse_transform(v, subject_number=7):
    inv_mod = modinv(v, 20201227)
    return inv_mod


def transform(v, subject_number, p=False):
    a = v * subject_number
    if p:
        print(f'v * subject_number = {a}')
    b = a % mod
    if p:
        print(f'% {mod} = {b}')
    return (v * subject_number) % 20201227


def ntransform(n, subject_number=7, target=None):
    if target:
        print(f'Trying to find {target}')
    v = 1
    for i in range(n):
        v = transform(v, subject_number)
        if target and v == target:
            print(f'target {target} found at loop {i + 1}')
            return i + 1
    return v


def altntransform(loopsize, subjectnumber=7, modulo=mod):
    t = 1
    for _ in range(loopsize):
        t *= subjectnumber
        t = t % modulo
    return t


assert ntransform(8) == 5764801
assert ntransform(11) == 17807724
assert altntransform(8) == 5764801
assert altntransform(11) == 17807724

assert altntransform(8306869) == 8614349
assert altntransform(6041183) == 8335663


def searchntransform(maxloopstart, subjectnumber=7, modulo=mod, targets=lines):
    t = 1
    for i in range(maxloopstart):
        if i % 10000 == 0:
            print(i)
        t *= subjectnumber
        t = t % modulo
        if t in targets:
            print('loooooop number')
            print(i)
    return t


# ntransform(11)
# a = inverse_transform(17807724)
# for i in range(20):
#     print(a*i)
#
# print(pow(17807724, -1, 20201227))

# searchntransform(100000000000000)

ek = altntransform(8306869, 8335663)
ek2 = altntransform(6041183, 8614349)
print(ek)
print(ek2)
assert ek == ek2

