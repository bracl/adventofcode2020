#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools

import utils

rules = utils.read_input('19.1')
messages = utils.read_input('19.2')

# rules = [
#     '42: 9 14 | 10 1',
#     '9: 14 27 | 1 26',
#     '10: 23 14 | 28 1',
#     '1: "a"',
#     '11: 42 31',
#     '5: 1 14 | 15 1',
#     '19: 14 1 | 14 14',
#     '12: 24 14 | 19 1',
#     '16: 15 1 | 14 14',
#     '31: 14 17 | 1 13',
#     '6: 14 14 | 1 14',
#     '2: 1 24 | 14 4',
#     '0: 8 11',
#     '13: 14 3 | 1 12',
#     '15: 1 | 14',
#     '17: 14 2 | 1 7',
#     '23: 25 1 | 22 14',
#     '28: 16 1',
#     '4: 1 1',
#     '20: 14 14 | 1 15',
#     '3: 5 14 | 16 1',
#     '27: 1 6 | 14 18',
#     '14: "b"',
#     '21: 14 1 | 1 14',
#     '25: 1 1 | 1 14',
#     '22: 14 14',
#     '8: 42',
#     '26: 14 22 | 1 20',
#     '18: 15 15',
#     '7: 14 5 | 1 21',
#     '24: 14 1',
# ]
# messages = [
#     'abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa',
#     'bbabbbbaabaabba',
#     'babbbbaabbbbbabbbbbbaabaaabaaa',
#     'aaabbbbbbaaaabaababaabababbabaaabbababababaaa',
#     'bbbbbbbaaaabbbbaaabbabaaa',
#     'bbbababbbbaaaaaaaabbababaaababaabab',
#     'ababaaaaaabaaab',
#     'ababaaaaabbbaba',
#     'baabbaaaabbaaaababbaababb',
#     'abbbbabbbbaaaababbbbbbaaaababb',
#     'aaaaabbaabaaaaababaa',
#     'aaaabbaaaabbaaa',
#     'aaaabbaabbaaaaaaabbbabbbaaabbaabaaa',
#     'babaaabbbaaabaababbaabababaaab',
#     'aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba',
# ]

rools = {}


class Rule:
    def __init__(self, r):
        self.r = r
        self.num = int(r.split(':', 1)[0])
        if '\"' in r:
            self.character = r[r.find('\"') + 1]
            self.rules = None
        else:
            self.character = None
            self.rules = [[int(y) for y in x.split()] for x in r.split(':', 1)[1].split('|')]
        self.combinations = None

    def combine(self):
        if self.character:
            self.combinations = [self.character]
        else:
            self.combinations = []
            for x in self.rules:
                parts = []
                for y in x:
                    why = rools[y]
                    combs = why.fetch_combinations()
                    parts.append(combs)
                combs = list(itertools.product(*parts))
                for c in combs:
                    j = ''.join(c)
                    self.combinations.append(j)

    def fetch_combinations(self):
        if not self.combinations:
            self.combine()
        return self.combinations


def part1():
    for r in rules:
        q = Rule(r)
        rools[q.num] = q

    rools[0].fetch_combinations()
    c = 0
    for m in messages:
        if m in rools[0].combinations:
            c += 1
    return c


# one = part1()

b = 0

rules.remove('8: 42')
rules.remove('11: 42 31')
rules.append('8: 42 | 42 8')
rules.append('11: 42 31 | 42 11 31')

print('Creating Rule dictionary')
for r in rules:
    q = Rule(r)
    rools[q.num] = q

rools[42].combine()
rools[31].combine()
# rools[11].combine()
# print(rools[31].r)
# print('\n')
# print(rools[98].r)
# print(rools[128].r)
# print(rools[109].r)
# print(rools[90].r)
# print('\n')
# print(rools[1].r)
# print(rools[70].r)
# print(rools[16].r)
# print(rools[68].r)

ft = rools[42]
to = rools[31]

# print(min([len(x) for x in ft.combinations]))
print('Caculating 8s combinations')
eight_combs = []
for x in ft.combinations:
    l = len(x)
    times = 96 // l
    for i in range(1, times + 1):
        e = x * i
        eight_combs.append(e)

rools[8].combinations = eight_combs

print('Caculating 11s combinations')
eleven_combs = []

# product1 = [''.join(x) for x in itertools.product(to.combinations, ft.combinations)]
bb = []
# for x in product1:
#     for y in ft.combinations:
#         for z in to.combinations:
#             bb.append(f'{y}{x}{z}')

# for x in to.combinations:
#     for y in ft.combinations:
#         l = len(x)
#         times = 96 // l
#         for i in range(1, times + 1):
#             e = x * i
#             eleven_combs.append(e)
#
# rools[11].combinations = eleven_combs


ftc = rools[42].combinations
toc = rools[31].combinations
# ec = rools[11].combinations

print('Making Regexes')
import re

ftr = f'(?:{"|".join(ftc)})'
tor = f'(?:{"|".join(toc)})'

regex_strings = [f'^{ftr}+{ftr}[{x}]{tor}[{x}]$'.replace('[', '{').replace(']', '}') for x in range(1, 6)]
regexes = [re.compile(rs) for rs in regex_strings]

print('Calculating 0s combinations')
rrr = rools[0]
c = 0
yyy = [y[:5] for y in ft.combinations]
for m in messages:
    for rs in regexes:
        if rs.match(m):
            c += 1
            break

print(c)

# a = [rr.match(m) for rr in [regex1,
#                             regex2,
#                             regex3,
#                             regex4,
#                             regex5]]
# i = [rm for rm in a if rm]
# if i:
#     ind = a.index(i[0])
#     print(f'Message {m} matches with regex {ind}')
#     c += 1
