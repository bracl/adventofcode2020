#!/usr/bin/env python
# -*- coding: utf-8 -*-

import utils

lines = utils.read_input('21.1')

# lines = [
#     'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)',
#     'trh fvjkl sbzzf mxmxvkd (contains dairy)',
#     'sqjhc fvjkl (contains soy)',
#     'sqjhc mxmxvkd sbzzf (contains fish)',
# ]

allegens = {}
ingredients = {}

foods = []

for food in lines:
    ingredient_list, allegen_list = food.split('(', 1)
    ings = ingredient_list.split()
    gens = allegen_list.replace('contains ', '').replace(')', '').split(', ')
    foods.append((ings, gens))
    for a in gens:
        if a in list(allegens):
            allegens[a].append(ings)
        else:
            allegens[a] = [ings]
    for i in ings:
        if i in list(ingredients):
            ingredients[i].append(gens)
        else:
            ingredients[i] = [gens]

definite = {}
suspect = {}
safe = []
for i, a in allegens.items():
    sets = [set(x) for x in a]
    head, *tail = sets
    if tail:
        common_ingredients = set(head).intersection(*tail)
        if len(common_ingredients) == 1:
            definite[i] = list(common_ingredients)[0]
        elif len(common_ingredients) > 1:
            suspect[i] = list(common_ingredients)
    else:
        suspect[i] = list(head)


def clean_sus():
    changes_made = True
    while changes_made:
        changes_made = False
        definites = definite.values()
        for k, v in suspect.items():
            for i in v:
                if i in definites:
                    suspect[k] = [b for b in v if b != i]
                    changes_made = True
        for k in list(suspect):
            if len(suspect[k]) == 1:
                definite[k] = suspect[k][0]
                del suspect[k]


clean_sus()
# print(definite)
# print(suspect)

c = 0
definite_all = list(definite)
definite_ing = definite.values()

for f in foods:
    sus = True
    for a in f[1]:
        know = a in definite_all
        sus = sus and not know
    if not sus:
        safe = [f for f in f[0] if f not in definite_ing]
        c += (len(safe))

# print(definite)

alls = list(definite)
alls.sort()
s = ''
for a in alls:
    s += f'{definite[a]},'
print(s)